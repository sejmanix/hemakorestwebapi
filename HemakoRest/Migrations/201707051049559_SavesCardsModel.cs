namespace HemakoRest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SavesCardsModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsRed = c.Boolean(nullable: false),
                        Time = c.Time(nullable: false, precision: 7),
                        Comment = c.String(),
                        PlayerId = c.Int(nullable: false),
                        GameId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Saves",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SaveType = c.Int(nullable: false),
                        IsSuccessful = c.Boolean(nullable: false),
                        IsPenalty = c.Boolean(nullable: false),
                        IsFreeKick = c.Boolean(nullable: false),
                        Time = c.Time(nullable: false, precision: 7),
                        Comment = c.String(),
                        PlayerId = c.Int(nullable: false),
                        GameId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Shots", "IsOpponentGoal", c => c.Boolean(nullable: false));
            AddColumn("dbo.Shots", "GameId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Shots", "GameId");
            DropColumn("dbo.Shots", "IsOpponentGoal");
            DropTable("dbo.Saves");
            DropTable("dbo.Cards");
        }
    }
}
