// <auto-generated />
namespace HemakoRest.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class MoveShotAction : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(MoveShotAction));
        
        string IMigrationMetadata.Id
        {
            get { return "201707050913227_MoveShotAction"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
