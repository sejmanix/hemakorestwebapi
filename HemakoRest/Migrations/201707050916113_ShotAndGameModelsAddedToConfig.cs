namespace HemakoRest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ShotAndGameModelsAddedToConfig : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Games",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Place = c.String(),
                        GoalsScored = c.Int(nullable: false),
                        GoalsLost = c.Int(nullable: false),
                        Opponent = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Shots",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OnGoal = c.Boolean(nullable: false),
                        FixedPartType = c.Int(nullable: false),
                        BodyType = c.Int(nullable: false),
                        IsGoal = c.Boolean(nullable: false),
                        Time = c.Time(nullable: false, precision: 7),
                        Comment = c.String(),
                        PlayerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Shots");
            DropTable("dbo.Games");
        }
    }
}
