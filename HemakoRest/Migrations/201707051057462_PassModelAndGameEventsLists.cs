namespace HemakoRest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PassModelAndGameEventsLists : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Passes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsAssist = c.Boolean(nullable: false),
                        IsSuccessful = c.Boolean(nullable: false),
                        FixedPartType = c.Int(nullable: false),
                        Time = c.Time(nullable: false, precision: 7),
                        Comment = c.String(),
                        PlayerId = c.Int(nullable: false),
                        GameId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Games", "TeamId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Games", "TeamId");
            DropTable("dbo.Passes");
        }
    }
}
