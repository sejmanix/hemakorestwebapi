// <auto-generated />
namespace HemakoRest.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class SavesCardsModel : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SavesCardsModel));
        
        string IMigrationMetadata.Id
        {
            get { return "201707051049559_SavesCardsModel"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
