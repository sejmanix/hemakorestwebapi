﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HemakoRest.Models.Enums
{
    public class FixedEnums
    {
        public enum PCFKick { None=0, Penalty=1, Free=2, Corner=3 };
        public enum BodyType { None = 0, LeftFoot =1, RightFoot=2, Head =3};

        public enum Position { None =0, Goalkeeper = 1, Defender = 2, Striker=3};

        public enum SaveType { None =0, Catch=1, KeeperIntervention=2, TouchOnCorner=3};
        
    }
}