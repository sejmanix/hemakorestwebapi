﻿using HemakoRest.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HemakoRest.Models
{
    public class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Nickname { get; set; }
        public FixedEnums.Position Position { get; set; }
        public int TeamId { get; set; }
        public bool IsActive { get; set; }

    }
}