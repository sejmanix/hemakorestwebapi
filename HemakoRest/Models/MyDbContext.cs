﻿using HemakoRest.Models.Events;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HemakoRest.Models
{
    public class MyDbContext : DbContext
    {
        public DbSet<Player> Players { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Shot> Shots { get; set; }
        public DbSet<Save> Saves { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Pass> Passes { get; set; }
        public MyDbContext()
            : base()
        {

        }
    }
}