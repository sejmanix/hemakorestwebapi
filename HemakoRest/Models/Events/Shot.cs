﻿using HemakoRest.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HemakoRest.Models.Events
{
    public class Shot : GameEvent
    {
        public bool OnGoal { get; set; }
        public FixedEnums.PCFKick FixedPartType{ get; set; }
        public FixedEnums.BodyType BodyType { get; set; }
        public bool IsGoal { get; set; }
        public bool IsOpponentGoal { get; set; }

    }
}