﻿using HemakoRest.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HemakoRest.Models.Events
{
    public class Pass : GameEvent
    {
        public bool IsAssist { get; set; }
        public bool IsSuccessful { get; set; }
        public FixedEnums.PCFKick FixedPartType{ get; set; }


    }
}