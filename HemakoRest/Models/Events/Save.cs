﻿using HemakoRest.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HemakoRest.Models.Events
{
    public class Save :GameEvent
    {
        public FixedEnums.SaveType SaveType { get; set; }
        public bool IsSuccessful { get; set; }
        public bool IsPenalty { get; set; }
        public bool IsFreeKick { get; set; }

    }
}