﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HemakoRest.Models.Events
{
    public abstract class GameEvent
    {
        public int Id { get; set; }
        public TimeSpan Time { get; set; }
        public string Comment { get; set; }
        public int PlayerId { get; set; }
        public int GameId { get; set; }
    }
}