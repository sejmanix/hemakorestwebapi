﻿using HemakoRest.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HemakoRest.Models
{
    public class Game
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Place { get; set; }
        public int GoalsScored { get; set; }
        public int GoalsLost { get; set; }
        public string Opponent { get; set; }
        public int UserId { get; set; }
        public int TeamId { get; set; }
        public IEnumerable<Shot> ShotEvents { get; set; }
        public IEnumerable<Save> SaveEvents { get; set; }
        public IEnumerable<Pass> PassEvents { get; set; }
        public IEnumerable<Card> CardEvents { get; set; }
        public IEnumerable<Player> Players { get; set; }

    }
}